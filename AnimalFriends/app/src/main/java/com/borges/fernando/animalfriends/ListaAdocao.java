package com.borges.fernando.animalfriends;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.borges.fernando.animalfriends.adapter.AdapterAdocao;
import com.borges.fernando.animalfriends.dao.AdocaoDAO;
import com.borges.fernando.animalfriends.modelos.Adocao;

import java.util.List;

public class ListaAdocao extends AppCompatActivity {
    private ListView listView;
    private List<Adocao> listaAdocao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_adocao);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView=(ListView) findViewById(R.id.listView2);
        listaAdocao = AdocaoDAO.getLista();
        listView.setAdapter(new AdapterAdocao(listaAdocao, this));
      /*  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }


    public void chamarAdocao(View view){
        Intent intent = new Intent(getApplicationContext(),NovaAdocao.class);
        startActivity(intent);
    }



}
