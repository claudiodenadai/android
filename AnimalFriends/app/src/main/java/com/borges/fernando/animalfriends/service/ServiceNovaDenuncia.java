package com.borges.fernando.animalfriends.service;

import android.os.StrictMode;

import com.borges.fernando.animalfriends.modelos.MausTratos;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class ServiceNovaDenuncia {
    public static void cadastrarNovaDenunciaWebService(MausTratos novaDenuncia) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String myurl = "http://172.17.238.163:8080/ServicoWeb/resource/WebService/add";
        String POST_PARAMS =
                "endereco" + novaDenuncia.getEndereco() +
                        "&descricao" + novaDenuncia.getDescricao() +
                        "&imagens" + novaDenuncia.getImagens();

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);

            OutputStream os = conn.getOutputStream();
            os.write(POST_PARAMS.getBytes());
            os.flush();
            os.close();

            conn.connect();
            int response = conn.getResponseCode();
            //Log.i("MainActivity", "The response is: " + response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
