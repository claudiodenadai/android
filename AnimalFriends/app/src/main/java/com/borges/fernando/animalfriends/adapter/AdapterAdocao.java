package com.borges.fernando.animalfriends.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.borges.fernando.animalfriends.R;
import com.borges.fernando.animalfriends.modelos.Adocao;


import java.util.List;


public class AdapterAdocao extends BaseAdapter {
List<Adocao> listaAdocoes;
    Activity activity;

    public AdapterAdocao(List<Adocao> listaAdocoes, Activity activity) {
        this.listaAdocoes = listaAdocoes;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return listaAdocoes.size();
    }

    @Override
    public Object getItem(int position) {
        return listaAdocoes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listaAdocoes.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      View view = LayoutInflater.from(activity).inflate(R.layout.adapter_adocao, parent, false);
        Adocao adocao = listaAdocoes.get(position);

        TextView textNomeAnimal = (TextView) view.findViewById(R.id.textNomeAnimal);
        textNomeAnimal.setText(adocao.getNomeAnimal());


        TextView textDescricaoAnimal = (TextView) view.findViewById(R.id.textDescricaoAnimal);
        textDescricaoAnimal.setText(adocao.getDescricaoAnimal());

return view;


    }



}
