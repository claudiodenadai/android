package com.borges.fernando.animalfriends.service;

import android.os.StrictMode;

import com.borges.fernando.animalfriends.modelos.Adocao;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class ServiceNovaAdocao {
    public static void cadastrarNovaAdocaoWebService(Adocao novaAdocao) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String myurl = "http://172.17.238.163:8080/ServicoWeb/resource/WebService/add";
        String POST_PARAMS =
                "nomeAnunciante" + novaAdocao.getNomeAnunciante() +
                        "&estado" + novaAdocao.getEstado() +
                        "&cidade" + novaAdocao.getCidade() +
                        "&telefone" + novaAdocao.getTelefone() +
                        "&observacao" + novaAdocao.getObservacao() +
                        "&nomeanimal" + novaAdocao.getNomeAnimal() +
                        "&descricaoanimal" + novaAdocao.getDescricaoAnimal() +
                        "&castracao" + novaAdocao.getCastracao() +
                        "&especie" + novaAdocao.getEspecie() +
                        "&idade" + novaAdocao.getIdade() +
                        "&imagens" + novaAdocao.getImagens() +
                        "&raca" + novaAdocao.getRaca() +
                        "&sexo" + novaAdocao.getSexo() +
                        "&pelagem" + novaAdocao.getPelagem() +
                        "&portepeso" + novaAdocao.getPortePeso();
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);

            OutputStream os = conn.getOutputStream();
            os.write(POST_PARAMS.getBytes());
            os.flush();
            os.close();

            conn.connect();
            int response = conn.getResponseCode();
            //Log.i("MainActivity", "The response is: " + response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
