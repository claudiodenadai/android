package com.borges.fernando.animalfriends;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.borges.fernando.animalfriends.dao.AdocaoDAO;
import com.borges.fernando.animalfriends.modelos.Adocao;
import com.borges.fernando.animalfriends.service.ServiceNovaAdocao;
public class NovaAdocao extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adocao);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

     /*   FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }


    public void cadastrarAdocao(View view) {

        //Anunciante
        EditText nomeAnun = (EditText) findViewById(R.id.nomeAnunciante);
        String nomeAnunc = nomeAnun.getText().toString();

        EditText telefoneAnun = (EditText) findViewById(R.id.telefoneAnunciante);
        String telefoneAnunc = telefoneAnun.getText().toString();

        EditText estadoAnun = (EditText) findViewById(R.id.estadoAnunciante);
        String estadoAnunc = estadoAnun.getText().toString();

        EditText cidadeAnun = (EditText) findViewById(R.id.cidadeAnunciante);
        String cidadeAnunc = cidadeAnun.getText().toString();


        //Animal
        EditText nomeAn = (EditText) findViewById(R.id.nomeAnimal);
        String nomeAni = nomeAn.getText().toString();


        EditText descAn = (EditText) findViewById(R.id.descricaoPet);
        String descAni = descAn.getText().toString();

        if (TextUtils.isEmpty(nomeAnunc)) {
            nomeAnun.setError("Informe um nome");
            nomeAnun.requestFocus();
            return;
        } else if (TextUtils.isEmpty(telefoneAnunc)) {
            telefoneAnun.setError("Informe um telefone");
            telefoneAnun.requestFocus();
            return;
        } else if (TextUtils.isEmpty(estadoAnunc)) {
            estadoAnun.setError("Informe um estado");
            estadoAnun.requestFocus();
            return;
        } else if (TextUtils.isEmpty(cidadeAnunc)) {
            cidadeAnun.setError("Informe uma cidade");
            cidadeAnun.requestFocus();
            return;
        } else if (TextUtils.isEmpty(nomeAni)) {
            nomeAn.setError("Informe um nome");
            nomeAn.requestFocus();
            return;
        } else if (TextUtils.isEmpty(descAni)) {
            descAn.setError("Dê uma descrição");
            descAn.requestFocus();
            return;

        } else {

            Adocao novaAdocao = new Adocao();
            novaAdocao.setNomeAnunciante(nomeAnunc);
            novaAdocao.setEstado(estadoAnunc);
            novaAdocao.setCidade(cidadeAnunc);
            novaAdocao.setTelefone(telefoneAnunc);
            novaAdocao.setNomeAnimal(nomeAni);
            novaAdocao.setDescricaoAnimal(descAni);
            novaAdocao.setCastracao(null);
            novaAdocao.setEspecie(null);
            novaAdocao.setIdade(null);
            novaAdocao.setImagens(null);
            novaAdocao.setObservacao(null);
            novaAdocao.setRaca(null);
            novaAdocao.setSexo(null);
            novaAdocao.setPelagem(null);
            novaAdocao.setPortePeso(null);
            novaAdocao.save();
            ServiceNovaAdocao.cadastrarNovaAdocaoWebService(novaAdocao);
            Toast.makeText(NovaAdocao.this, "Cadastrado" + novaAdocao.getId(), Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
