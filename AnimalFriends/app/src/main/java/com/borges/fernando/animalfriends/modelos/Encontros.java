package com.borges.fernando.animalfriends.modelos;

import com.orm.SugarRecord;

import java.util.List;


public class Encontros extends SugarRecord {
    private String proprietario;
    private Integer fone;
    private String email;
    private String estado;
    private String cidade;
    private String sexo;
    private String especie;
    private String raca;
    private String porte;
    private String observacoes;
    private Boolean certificado;
    private Integer idade;
    private List<Integer> imagens;

    public Encontros() {
    }

    public Encontros(String proprietario, Integer fone, String email, String estado, String cidade, String sexo, String especie, String raca, String porte, String observacoes, Boolean certificado, Integer idade, List<Integer> imagens) {
        this.proprietario = proprietario;
        this.fone = fone;
        this.email = email;
        this.estado = estado;
        this.cidade = cidade;
        this.sexo = sexo;
        this.especie = especie;
        this.raca = raca;
        this.porte = porte;
        this.observacoes = observacoes;
        this.certificado = certificado;
        this.idade = idade;
        this.imagens = imagens;
    }

    public String getProprietario() {
        return proprietario;
    }

    public void setProprietario(String proprietario) {
        this.proprietario = proprietario;
    }

    public Integer getFone() {
        return fone;
    }

    public void setFone(Integer fone) {
        this.fone = fone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public String getPorte() {
        return porte;
    }

    public void setPorte(String porte) {
        this.porte = porte;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Boolean getCertificado() {
        return certificado;
    }

    public void setCertificado(Boolean certificado) {
        this.certificado = certificado;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public List<Integer> getImagens() {
        return imagens;
    }

    public void setImagens(List<Integer> imagens) {
        this.imagens = imagens;
    }
}
