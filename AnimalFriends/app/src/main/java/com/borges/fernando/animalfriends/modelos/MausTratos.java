package com.borges.fernando.animalfriends.modelos;

import com.orm.SugarRecord;

import java.util.List;


public class MausTratos extends SugarRecord{
    /*private Long id;*/
    private String endereco;
    private Integer latitude;
    private Integer longitude;
    private String descricao;
    private List<Integer> imagens;

    public MausTratos(){

    }

    public MausTratos(String endereco, Integer latitude, Integer longitude, String descricao, List<Integer> imagens) {

        this.endereco = endereco;
        this.latitude = latitude;
        this.longitude = longitude;
        this.descricao = descricao;
        this.imagens = imagens;
    }


    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Integer getLatitude() {
        return latitude;
    }

    public void setLatitude(Integer latitude) {
        this.latitude = latitude;
    }

    public Integer getLongitude() {
        return longitude;
    }

    public void setLongitude(Integer longitude) {
        this.longitude = longitude;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Integer> getImagens() {
        return imagens;
    }

    public void setImagens(List<Integer> imagens) {
        this.imagens = imagens;
    }
}
