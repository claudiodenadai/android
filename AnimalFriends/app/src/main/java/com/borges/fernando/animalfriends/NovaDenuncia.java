package com.borges.fernando.animalfriends;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.borges.fernando.animalfriends.dao.MausTratosDAO;
import com.borges.fernando.animalfriends.modelos.MausTratos;
import com.borges.fernando.animalfriends.service.ServiceNovaDenuncia;

public class NovaDenuncia extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_denuncia);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void efetuarDenuncia(View view) {
        EditText txEndereco = (EditText) findViewById(R.id.endereco);
        EditText txDescricao = (EditText) findViewById(R.id.descricao);
        String stEndereco = txEndereco.getText().toString();
        String stDescricao = txDescricao.getText().toString();

        if (TextUtils.isEmpty(stEndereco)) {
            txEndereco.setError("Informe um endereço");
            txEndereco.requestFocus();
            return;
        } else if (TextUtils.isEmpty(stDescricao)) {
            txDescricao.setError("Dê uma descrição");
            txEndereco.requestFocus();
            return;
        }
        else {

            MausTratos denuncia = new MausTratos();
            denuncia.setEndereco(stEndereco);
            denuncia.setDescricao(stDescricao);
            denuncia.setImagens(null);
            denuncia.setLatitude(null);
            denuncia.setLongitude(null);
            denuncia.save();
            ServiceNovaDenuncia.cadastrarNovaDenunciaWebService(denuncia);
            Toast.makeText(NovaDenuncia.this, "Cadastrado"+denuncia.getId(), Toast.LENGTH_SHORT).show();
            finish();
        }



    }
}
