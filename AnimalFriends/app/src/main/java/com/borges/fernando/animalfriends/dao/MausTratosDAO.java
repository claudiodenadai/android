package com.borges.fernando.animalfriends.dao;

import com.borges.fernando.animalfriends.modelos.MausTratos;

import java.util.ArrayList;
import java.util.List;


public class MausTratosDAO {
    private static List<MausTratos> listaMausTratos = new ArrayList<>();
    private static Long id = 0L;

    public static void adicionar(MausTratos mausTratos) {
        listaMausTratos.add(mausTratos);
    }

    public static void remover(MausTratos mausTratos) {
        listaMausTratos.remove(mausTratos);
    }

    public static List<MausTratos> getLista() {
        return listaMausTratos;

    }

    public static Long getIdIncrement() {
        return ++id;
    }
}
