package com.borges.fernando.animalfriends.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.borges.fernando.animalfriends.R;
import com.borges.fernando.animalfriends.modelos.MausTratos;

import java.util.List;

/**
 * Classe responsavel por preencher a listView
 */
public class AdapterDenuncias extends BaseAdapter {
List<MausTratos> listaMausTratos;
    Activity activity;

    public AdapterDenuncias(List<MausTratos> listaMausTratos, Activity activity) {
        this.listaMausTratos = listaMausTratos;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return listaMausTratos.size();
    }

    @Override
    public Object getItem(int position) {
        return listaMausTratos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listaMausTratos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(activity).inflate(R.layout.adapter_denuncias, parent,false);
        MausTratos mausTratos = listaMausTratos.get(position);

        TextView textDescricao = (TextView) view.findViewById(R.id.textDescricao);
        textDescricao.setText(mausTratos.getDescricao());

        TextView textEndereco = (TextView) view.findViewById(R.id.textEndereco);
        textEndereco.setText(mausTratos.getEndereco());

        return view;
    }
}
