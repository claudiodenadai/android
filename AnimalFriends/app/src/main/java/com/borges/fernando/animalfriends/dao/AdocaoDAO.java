package com.borges.fernando.animalfriends.dao;

import com.borges.fernando.animalfriends.modelos.Adocao;

import java.util.ArrayList;
import java.util.List;

public class AdocaoDAO {
    private static List<Adocao> listaAdocao = new ArrayList<>();
    private static Long id = 0L;

    public static void adicionar(Adocao adocao) {
        System.out.println("Parametros Adoção"+adocao);
        listaAdocao.add(adocao);
    }

    public static void remover(Adocao adocao) {
        listaAdocao.remove(adocao);
    }

    public static List<Adocao> getLista() {
        return listaAdocao;

    }

    public static Long getIdIncrement() {
        return ++id;
    }
}
